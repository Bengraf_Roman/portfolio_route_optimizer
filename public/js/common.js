var admin = {};
var markers = [];
admin.transport = function (controller, method, data, callback) {
    if (!controller || !method) {
        return;
    }
    $.ajax({
        type: "POST",
        url: controller + "/ajax/" + method,
        data: data,
        success: function (msg) {
            if (callback) {
                callback(JSON.parse(msg));
            }
        }
    });
};

admin.handler = function (data) {
    console.log(data);
};

admin.auth = function (data) {
    if (data.result) {
        location.reload();
    } else {
        console.log('Auth failed');
    }
};

admin.logout = function (response) {
    if (response.result) {
        location.reload();
    } else {
        console.log('Logout failed');
    }
};

admin.renderResult = function (response) {
    var target_selector = response.target;
    if (target_selector) {
        var target_obj = $('[data-target="' + target_selector + '"]');
        target_obj.html(response.result);
    }
};

admin.renderWaypoints = function () {
    admin.transport('index', 'renderWaypoint', {json_way_points: $('#json_input')[0].value}, admin.renderResult);
};


admin.deletePoint = function (id) {
    $('[data-row-id="' + id + '"]').remove();
    admin.refreshList();
}

admin.getPauses = function () {
    var way_points = JSON.parse($('#json_input')[0].value).way_points;
    var result = [];
    for (var i in way_points) {
        result.push(way_points[i].pause);
    }
    return result;
}

admin.refreshList = function () {
    var form_data = admin.serializeAssoc($("#way_points_form"));
    var _way_points = [];
    for (var i in form_data.way_points) {
        _way_points.push(form_data.way_points[i]);
    }
    form_data.way_points = _way_points;

    console.log(form_data);
    $('#json_input')[0].value = JSON.stringify(form_data);

    admin.calulateRoute();
}

admin.currentDayTimeStamp = function (secondsOfDay) {
    var now = new Date();
    var startOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    return timestamp = secondsOfDay + startOfDay / 1000;
}
admin.renderRoute = function (response) {
    var origin = response.origin;
    var destination = response.destination;
    var waypoints = response.waypoints;
    admin.renderWaypoints();
    displayRoute(origin, destination, waypoints, admin.directionsService, admin.directionsDisplay)
}

admin.calulateRoute = function () {
    admin.transport('index', 'renderRoute', {json_way_points: $('#json_input')[0].value}, admin.renderRoute);
}

admin.directions = function () {

    this.map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: {lat: 51.3006897, lng: 37.8400472}
    });
    this.directionsService = new google.maps.DirectionsService;

    this.directionsDisplay = new google.maps.DirectionsRenderer({
        draggable: true,
        map: admin.map,
        panel: document.getElementById('right-panel')
    });
    admin.directionsDisplay.addListener('directions_changed', function () {
        var dirs = admin.directionsDisplay.getDirections();
        computeTotalDistance(dirs);
        computeTotalDuration(dirs);
        admin.changePoint();
    });
}

admin.changeSortOfPoints = function () {

}

admin.changePoint = function () {
    var np = [];
    var inp = $('#json_input')[0];
    var directions = admin.directionsDisplay.getDirections();
    np.push(directions.request.origin);
    for (var wp in directions.request.waypoints) {
        np.push(directions.request.waypoints[wp].location);
    }
    np.push(directions.request.destination)
    this.new_points = np;

    var old_data = JSON.parse(inp.value);
    var current_point_index = directions.request.Yb;

    var new_lat = admin.new_points[current_point_index].lat();
    var new_lng = admin.new_points[current_point_index].lng();

    old_data.way_points[current_point_index].lat = new_lat;
    old_data.way_points[current_point_index].lng = new_lng;


    old_data.way_points[current_point_index].manual = true;
    inp.value = JSON.stringify(old_data);
    old_data = {};
    admin.renderWaypoints();
    admin.calulateRoute();
}

admin.setMarker = function (LngLat, content) {
    var coordInfoWindow = new google.maps.InfoWindow();
    coordInfoWindow.setContent(content);
    coordInfoWindow.setPosition(LngLat);
}

admin.mapPins = function (action) {
    var start_point = admin.directionsDisplay.getDirections().request.origin;
    var points = admin.directionsDisplay.getDirections().routes[0].legs;
    var str_start_time = JSON.parse($('#json_input')[0].value).start
    if (!str_start_time) {
        str_start_time = '00:00';
    }
    var start_time = sdeformat(str_start_time);
    var legs = admin.directionsDisplay.getDirections().routes[0].legs;
    var content = '<b style="color:#5fe662">Старт: ' + sformat(start_time) + '</b>';
    var arrival = start_time;
    if (action == "show") {
        addMarker(new google.maps.LatLng(start_point.lat, start_point.lng), admin.map, content);
        for (var i in points) {
            arrival += legs[i].duration.value;
            content = '<span class="badge">' + (i * 1 + 1) + '</span><br>Прибытие: ' + sformat(arrival) + '<br>';
            arrival += admin.getPauses()[i] * 60;
            content += 'Разгрузка: ' + admin.getPauses()[i] + '<br>';

            content += 'Отправка: ' + sformat(arrival);
            if (i == (points.length - 1)) {
                content += '<br><i class="fa fa-flag-checkered"></i> <b style="color: #ec971f">ФИНИНШ</b>';
            }

            addMarker(points[i].end_location, admin.map, content);
        }
    } else {
        clearMarkers();
    }
}

function addMarker(location, map, content) {
    // Add the marker at the clicked location, and add the next-available label
    // from the array of alphabetical characters.
    var marker = new google.maps.Marker({
        position: location,
        label: '',
        map: map
    });
    var infowindow = new google.maps.InfoWindow({
        content: content
    });
    marker.addListener('click', function () {
        infowindow.open(map, marker);
    });
    infowindow.open(map, marker);
    markers.push(marker);
}

function toggle(ident, action) {
    var toggle_on = $('[data-toggle-on=' + ident + ']');

    var toggle_off = $('[data-toggle-off=' + ident + ']');
    if (action == 'on') {
        toggle_on.hide();
        toggle_off.show();
    } else if (action == 'off') {
// Sets the map on all markers in the array.
// Removes the markers from the map, but keeps them in the array.
// Shows any markers currently in the array.
// Deletes all markers in the array by removing references to them.
        toggle_off.hide();
        toggle_on.show();
    }
}

function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

function clearMarkers() {
    setMapOnAll(null);
}

function showMarkers() {
    setMapOnAll(map);
}

function deleteMarkers() {
    clearMarkers();
    markers = [];
}


function initMap() {
    admin.directions();
}



function displayRoute(origin, destination, waypoints, service, display) {
    var _waypoints = [];
    for (var wp in waypoints) {
        _wp = waypoints[wp];
        var LatLng = new google.maps.LatLng(_wp.lat, _wp.lng);
        _waypoints.push({
            location: LatLng,
            stopover: true
        });
        admin.setMarker(LatLng, _wp.name);
    }

    admin.setMarker(origin, 'START');
    admin.setMarker(destination, 'END');

    service.route({
        origin: origin,
        destination: destination,
        waypoints: _waypoints,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.DRIVING,
        drivingOptions: {
            departureTime: new Date(admin.currentDayTimeStamp(21600)*1000)
        },
        avoidTolls: true
    }, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            admin.directionsDisplay.setDirections(response);
            var route = response.routes[0];
        } else {
            alert('Что-то пошло не так с апи...');
        }
    });
}

function computeTotalDistance(result) {
    var total = 0;
    var myroute = result.routes[0];
    for (var i = 0; i < myroute.legs.length; i++) {
        total += myroute.legs[i].distance.value;
    }
    total = total / 1000;
    document.getElementById('total').innerHTML = total + ' км';
}

function computeTotalDuration(result) {
    var total = 0;
    var total_full = 0;
    var myroute = result.routes[0];
    var pauses = admin.getPauses();
    for (var i = 0; i < myroute.legs.length; i++) {
        total += myroute.legs[i].duration.value;
        total_full = total + (pauses[i] * 60) * (i + 1);
    }

    document.getElementById('total_duration').innerHTML = 'Движение: ' + sformat(total) + ' / Всего: ' + sformat(total_full);
}

function sformat(s) {
    var fm = [
        Math.floor(s / 60 / 60) % 24, // HOURS
        Math.floor(s / 60) % 60, // MINUTES
        s % 60 // SECONDS
    ];
    return $.map(fm, function (v, i) {
        return ((v < 10) ? '0' : '') + v;
    }).join(':');
}

function sdeformat(string) {
    var a = string.split(':');
    return (+a[0]) * 60 * 60 + (+a[1]) * 60;
}

admin.serializeAssoc = function (arr) {
    var data = {};
    $.each(arr.serializeArray(), function (key, obj) {
        var a = obj.name.match(/(.*?)\[(.*?)\]/);
        if (a !== null) {
            var subName = a[1];
            var subKey = a[2];
            if (!data[subName]) data[subName] = [];
            if (!data[subName][data[subName].length + 1]) data[subName][data[subName].length + 1] = JSON.parse(obj.value);
        } else {
            if (data[obj.name]) {
                if ($.isArray(data[obj.name])) {
                    data[obj.name].push(obj.value);
                } else {
                    data[obj.name] = [];
                    data[obj.name].push(obj.value);
                }
            } else {
                data[obj.name] = obj.value;
            }
        }
    });
    return data;
};


/**
 * DOWNLOAD
 *
 *
 */

admin.dl = function (response) {
    var a = document.createElement('a');
    a.href = response.file_url;
    a.target = '_blank';
    a.download = response.file_name;
    a.style.display = 'none';
    document.body.appendChild(a);
    a.click();
    delete a;
}