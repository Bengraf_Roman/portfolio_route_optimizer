<?php
namespace App\Mvc\Model;
use App\Mvc\Controller\ErrorController;

/**
 * Класс работы с API
 * Class Api
 * @package App\Mvc\Model
 */
class Api extends BaseModel
{
    protected static $data_path = 'auth/api_data.json';
    private static $data;

    public static function getData()
    {
        $data_json = file_get_contents(self::$data_path);
        static::$data = json_decode($data_json, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }

    public static function getService($service_key)
    {
        self::getData();
        $service = (object)static::$data[array_search($service_key, static::$data)];
        return $service ? (object)$service->{$service_key} : ErrorController::setError('Некорректный сервис API');
    }

    public static function getServiceKey($service_key)
    {
        return self::getService($service_key)->key;
    }
}