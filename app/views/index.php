<div class="row">
    <div class="col-sm-2 col-sm-offset-10">
        <button type="button" class="btn btn-default bg-purple"
                onclick="admin.transport('user','logout',{},admin.logout)">
            LOGOUT
        </button>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <div class="box box-feedback">
            <div class="box-header">
                JSON Waypoints
            </div>
            <div class="box-body">
                <textarea id="json_input" name="way_points" style="box-sizing: border-box; width:100%; height: 150px"
                          onchange="admin.transport('index', 'renderRoute', {json_way_points: $('#json_input')[0].value}, admin.renderRoute)"><?php echo $template; ?></textarea>
            </div>
            <div class="box-footer text-justify">

                <i class="fa fa-cloud-download pointer btn btn-xs bg-grey"
                   onclick="admin.transport('index','downloadJson', { content : $('#json_input')[0].value}, admin.dl)">
                </i>

                <i class="fa fa-calculator pointer btn btn-xs btn-warning pull-right"
                   onclick="admin.transport('index', 'renderRoute', {json_way_points: $('#json_input')[0].value}, admin.renderRoute)">
                </i>
            </div>

        </div>
        <div class="box box-feedback">
            <div class="box-header">
                Точки
            </div>
            <div class="box-body">
                <div data-target="way_points"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-9">
        <div class="box box-feedback">
            <div class="box-header">
                <h4>Map

                    <span class="pull-right">
                    <span>Метки</span>
                    <i data-toggle-on="pins" class="fa fa-toggle-off pointer"
                       onclick="admin.mapPins('show'); toggle('pins','on');"></i>
                    <i data-toggle-off="pins" class="fa fa-toggle-on pointer" style="display: none"
                       onclick="admin.mapPins('hide');toggle('pins','off');"></i>
                </span>
                </h4>
            </div>
            <div class="box-body" style="height: 500px;">
                &nbsp;
                <div id="map" style="height: 450px">

                </div>
            </div>
            <div class="box-footer">
                <div id="total"></div>
                <div id="total_duration"></div>
            </div>
        </div>

        <div class="box box-feedback">
            <div class="box-header">
                Детали
            </div>
            <div class="box-body">
                <div id="right-panel">

                </div>
            </div>
            <div class="box-footer">

            </div>

        </div>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=<?= $api_service ?>&callback=initMap" async defer></script>

<script>
    $(document).ready(function () {
        admin.renderWaypoints();
        admin.transport('index', 'renderRoute', {json_way_points: $('textarea')[0].value}, admin.renderRoute);
    });
</script>