<?php
namespace App\Mvc\Controller;

use App\Mvc\Model\Api;

class IndexController extends BaseController
{
    public function indexAction()
    {
        $request_template = $this->request->getQuery('wp');

        $api_service = Api::getServiceKey('google_maps');
        $this->load->view('index', [
            'api_service' => $api_service,
            'template' => $request_template ? $request_template : json_encode(json_decode(file_get_contents('auth/template.json'), true), JSON_UNESCAPED_UNICODE)
        ]);
    }

    public function renderWaypointAjax()
    {
        if (!$json_way_points = $this->request->getPost('json_way_points')) {
            return [];
        }
        $way_points = json_decode($json_way_points, true);

        $this->setResult(
            [
                'result' =>
                    $this->load->tplGet('way-point', [
                        'way_points' => $way_points['way_points'],
                        'car' => $way_points['car'],
                        'start' => $way_points['start']
                    ]),
                'target' => 'way_points'
            ]
        );
    }

    public function renderRouteAjax()
    {
        $way_points = json_decode($this->request->getPost('json_way_points'), true);
        foreach ($way_points['way_points'] as $k => $v) {
            unset($way_points[$k]['name']);
        }
        $origin = array_shift($way_points['way_points']);
        $destination = array_pop($way_points['way_points']);

        $this->setResult([
            'origin' => $origin,
            'destination' => $destination,
            'waypoints' => $way_points['way_points'],
        ]);
    }

    public function downloadJsonAjax()
    {
        $content = $this->request->getPost('content');
        if ($content && is_string($content)) {
            $file_path = 'public/uploads/';
            $file_name = 'result_' . time() . '.json';

            if (!@mkdir($file_path) && !is_dir($file_path)) {
                throw new \Exception('Problems with creating of temporary export directory.');
            }
            file_put_contents($file_path . $file_name, $content);
            $this->setResult([
                'file_url' => $file_path . $file_name,
                'file_name' => $file_name
            ]);
        } else {
            ErrorController::setError('Невалидные данные для сохранения...');
        }
    }


}