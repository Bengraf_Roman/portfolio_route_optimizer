#### Simple Route-Optimizer (GoogleMaps.API based)
##### July, 2015

### Description

This is a very simple application to optimize the route of movement during the day. Developed at the request of the shipping department. Everything is very simple! Incoming data - json-array of objects from the point at which you need to arrive (in no particular order, a list), as well as downtime - Output: built route optimized, with the expectation veremeni and distances.

### Back-End

This homemade framework on PHP 7. Designed according to the MVC paradigm. It, of course, there are downsides: no autoloader, in this case - is not the ORM, a template, the composer, but it's fast - 50ms on a home PC (front-end and back-end).

### Front-end
Nothing special. AJAX(jQuery), try to use OOP-paradigm in JS, GoogleMaps. JS-Api. 

#### To test:

Login: root
Password: root :)

#### My personal rating:

6 / 10

#### Nginx settings:


```
#!nginx

location / {
    rewrite ^/(.*)/?$ /index.php?uri=$1 last;
}
location ~* \.(?:css|js|jpe?g|gif|png)$ { }
location ~ \.php$ {
    include snippets/fastcgi-php.conf;
    fastcgi_pass unix:/run/php/php7.0-fpm.sock;
}
```